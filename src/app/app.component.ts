import { Component } from '@angular/core';
import { AuthFirebaseService } from './providers/@auth/auth-firebase.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  isLogged = false;

  constructor(
    private _auth: AuthFirebaseService,
    private router: Router
  ) {
    this._auth.currentUser.subscribe((res: firebase.User) => {
      if (res) {
        this.isLogged = true;
        this.router.navigate(['/list']);
      } else {
        this.isLogged = false;
        this.router.navigate(['/login']);
      }
    });
  }

  cerrarSesion() {
    Swal.fire({
      title: 'Confirmación',
      text: '¿Está seguro de cerrar sesión?',
      type: 'question',
      confirmButtonText: 'Cerrar',
      cancelButtonText: 'Cancelar',
      showCancelButton: true
    }).then((res) => {
      if (res.value) {
        this._auth.closeSession().then(()=>{
          Swal.fire('Exitoso', 'La sessión de usuario fue cerrada', 'success');
        });
      }
    });
  }

}
