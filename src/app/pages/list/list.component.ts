import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Promise } from 'q';
import { ModelService } from '../../providers/@data/model.service';
import { AuthFirebaseService } from '../../providers/@auth/auth-firebase.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  tasks: any[] = [];
  taskValue: string;
  currentUser: firebase.User;

  constructor(
    private _snackBar: MatSnackBar,
    private _model: ModelService,
    private _auth: AuthFirebaseService
  ) {

    this._auth.currentUser.subscribe((user: firebase.User) => {
      this.currentUser = user;
      this.getUserTasks(user.uid);
    });
  }


  getUserTasks(id) {
    this._model.colWithIds$('tasks', ref => ref.where('user', '==', id)).subscribe((resp: any[]) => {
      this.tasks = resp;
    });
  }

  onPress(evt) {
    if (evt.key === 'Enter') {
      this.addTask(this.taskValue);
      this.taskValue = '';
    }
  }

  addTask(value) {

    this.validateTask().then((res => {

      const dataTask = {
        text: value,
        complete: false,
        user: this.currentUser.uid
      };

      this._model.add('tasks', dataTask).then(() => {
        this.showMessage('Tarea creada exitosamente');
      }).catch((error) => {
        this.showMessage(error);
      });

    })).catch(error => {
      this.showMessage(error);
    });
  }

  showMessage(msg: string) {
    this._snackBar.open(msg, '', {
      duration: 1000
    });
  }

  validateTask(): Promise<any> {
    return Promise((resolve, reject) => {
      if (this.taskValue === '' || this.taskValue === undefined || this.taskValue == null) {
        reject('Tarea vacia');
      } else {
        resolve(true);
      }
    });
  }

  completeTasks(list) {


    Swal.fire({
      title: 'Confirmación',
      text: '¿Está seguro de marcar como completa(s) esta(s) tarea(s)?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Completar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        list._selected.forEach((element: any) => {

          this._model.update(`tasks/${element._value}`, {
            complete: true
          }).then(() => {
            this.showMessage('Tareas marcadas como realizadas');
          }).catch((error) => {
            this.showMessage(error);
          });
        });
      }
    });
  }

  deleteTask(list) {

    Swal.fire({
      title: 'Confirmación',
      text: '¿Está seguro de eliminar esta(s) tarea(s)?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        list._selected.forEach(element => {
          this._model.delete(`tasks/${element._value}`).then(() => {
            this.showMessage('Tareas eliminadas');
          }).catch((error) => {
            this.showMessage(error);
          });
        });
      }
    });

  }

}
