import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthFirebaseService } from '../../providers/@auth/auth-firebase.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent {

  resetForm = this.fb.group({
    email: [null, [Validators.required, Validators.email]],
  });

  constructor(
    private fb: FormBuilder,
    private _authService: AuthFirebaseService,
    private router: Router
  ) { }

  onSubmit() {
    this._authService.resetPassword(this.resetForm.value.email).finally(() => {
      Swal.fire(
        'Exito',
        'Hemos enviado un enlace al correo electrónico para recuperar su contraseña',
        'success'
      ).then( () => {
        this.gotoIniciarSesion();
      });

    });
  }

  gotoIniciarSesion() {
    this.router.navigate(['/login']);
  }

}
