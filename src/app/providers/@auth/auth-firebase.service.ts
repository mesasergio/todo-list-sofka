import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthFirebaseService {

  private user: Observable<firebase.User | null>;

  constructor(
    private _auth: AngularFireAuth
  ) {
    this.user = this._auth.authState;
  }

  // Obtenemos el estado del usuario autenticado
  get authenticated(): boolean {
    return this.user != null;
  }

  // Obtenemos el usuario actual
  get currentUser(): Observable<firebase.User | null> {
    return this.user;
  }
   // Ingreso con email
  signInWithEmail(email, pass): Promise<firebase.auth.UserCredential> {
    return this._auth.auth.signInWithEmailAndPassword(email, pass);
  }

  signUpWithEmail(email, pass): Promise<firebase.auth.UserCredential> {
    return this._auth.auth.createUserWithEmailAndPassword(email, pass);
  }

  resetPassword(email): Promise<void> {
    console.log(email);
    return this._auth.auth.sendPasswordResetEmail(email);
  }

  closeSession(): Promise<void> {
    return this._auth.auth.signOut();
  }


}
